This is a subclass of UIButton that includes a method that takes a block and executes it on a given UIControlEvent.

This eliminates the need to create selectors for responding to button events.
